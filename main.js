(function(){

	let video = document.getElementsByTagName('video')[0];
	let range = document.getElementById('range');
	let speed = document.getElementById('speed');
	let messages = document.getElementById('messages');
	let block_start = document.getElementById('block_start');
	let block_end = document.getElementById('block_end');
	let block_remove = document.getElementById('block_remove');
	let cut_video = document.getElementById('cut_video');
	let load_video = document.getElementById('load_video');
	let timer = document.getElementById('timer');

	let start_point = 0;
	let time_array = [];

	range.value = 0;
	video.src = null;
	video.controls = true;
	video.seeking = true;
	video.volume = 0.1;
	video.currentTime = 0;
	video.playbackRate = speed.value;
	block_end.disabled = true;
	block_remove.disabled = true;
	cut_video.disabled = true;

	let sendMessage = (txt) => {
		messages.innerHTML = txt;
	};


	block_start.onclick = () => {
		sendMessage('Начало блока ' + video.currentTime + ' мс');

		start_point = video.currentTime;

		let div = document.createElement('div');
		div.id = 'block_point';
		div.style.left = ((video.currentTime / video.duration) * 100) + '%';
		timer.appendChild(div);

		block_end.disabled = false;
	};


	block_end.onclick = () => {
		sendMessage('Конец блока ' + video.currentTime + ' мс');

		let block_point = document.getElementById('block_point');

		if (block_point){

			let point = (video.currentTime / video.duration) * 100;
			let width = point - ((start_point / video.duration) * 100);

			let div = document.createElement('div');
			div.className = 'block';
			div.id = 'block_id_' + time_array.length;
			div.setAttribute('block_id', time_array.length);
			div.style.left = block_point.style.left;
			div.style.width = width + '%';
			timer.appendChild(div);

			time_array.push({
				from: start_point.toFixed(1),
				to: point.toFixed(1)
			});

			block_point.remove();
			start_point = 0;
			block_end.disabled = true;
			cut_video.disabled = false;
		}
	};

	video.ontimeupdate = () => {
		range.value = video.currentTime * 1000;
	};

	video.onloadeddata = () => {
		range.max = video.duration * 1000;
	};

	speed.onchange = () => {
		video.playbackRate = speed.value;
	};

	range.oninput = () => {
		console.log(range.value / 1000);
		video.currentTime = range.value / 1000;
	};

	timer.onclick = (el) => {
		block_remove.disabled = !(el.target.className === 'block');
		block_remove.onclick = () => {
			let element = document.getElementById(el.target.id);;
			delete time_array[el.target.getAttribute('block_id')];
			element.parentNode.removeChild(element);
			if (document.getElementsByClassName('block').length === 0){
				cut_video.disabled = true;
			}
			block_remove.disabled = true;
		};
	};

	cut_video.onclick = () => {
		cut_video.disabled = true;
		sendMessage('Начало обрезки');
		fetch('/time', {
			method: 'post',
			body: JSON.stringify({time: time_array})
		}).then((data) => {
			return data.json();
		}).then((data) => {
			sendMessage(data['message']);
		});
	};

	(() => {
		fetch('/file').then((data) => {
			return data.json();
		}).then((data) => {
			video.src = data['file'];
		});
	})();


	document.addEventListener('keydown', (event) => {
		event.code === 'ArrowRight' ? range.value++ : false;
		event.code === 'ArrowLeft' ? range.value-- : false;
		event.code === 'Space' ? (video.paused ? video.play() : video.pause()) : false; 
		event.code === 'ArrowUp' ? speed.value++ : false;
		event.code === 'ArrowDown' ? speed.value-- : false;
	});

})();