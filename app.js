const { spawn } = require('child_process');

let start = () => {

	const path = require('path');
	const hostname = '0.0.0.0';
	const port = 8080;
	const fs = require('fs');
	const video_file = process.argv[2];
	const video_file_name = path.basename(video_file);

	let server = require('http').createServer((req, res) => {

		let cut = () => {
			let body = '';
			req.on('data', (data) => { body += data.toString(); });
			req.on('end', () => {
				let time_between = JSON.parse(body)['time'].map((el) => `between(t,${el.from},${el.to})`).join('+');

				let command = `ffmpeg -i ` + video_file + ` -vf "select='` + time_between + `',setpts=N/FRAME_RATE/TB" -af "aselect='` + time_between + `',asetpts=N/SR/TB" ` + 'out_' + video_file_name;

				spawn('rm -f ./out_' + video_file_name, [], { shell: true }).on('exit', () => {

					spawn(command, [], { shell: true }).on('exit', () => {
						console.log('Обрезка закончена. Файл: out_' + video_file_name);
						res.end(JSON.stringify({ message: 'Обрезка закончена. Файл: out_' + video_file_name }));
					});

				});

			});
		};


		switch(req.url){
			case video_file: fs.createReadStream(video_file).pipe(res); break;
			case '/main.js': fs.createReadStream('main.js').pipe(res); break;
			case '/time': cut(); break;
			case '/file': res.end(JSON.stringify({ file: video_file })); break;
			default: fs.createReadStream('index.html').pipe(res);
		}

	}).listen(port, hostname, () => {
		console.log(`Сервер работает: http://${hostname}:${port}/`);
	});

}

spawn('which', ['ffmpeg']).on('exit', (code) => {

	if (code > 0) {
		console.log('ffmpeg не найден');
		process.exit();
	}

	start();
});